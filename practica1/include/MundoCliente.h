// Mundo.h: interface for the CMundo class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_MUNDO_H__9510340A_3D75_485F_93DC_302A43B8039A__INCLUDED_)
#define AFX_MUNDO_H__9510340A_3D75_485F_93DC_302A43B8039A__INCLUDED_

#include <vector>
#include "Plano.h"



#include<stdlib.h>
#include<string.h>
#include<sys/types.h>
#include<fcntl.h>
#include<unistd.h>
#include<sys/stat.h>
#include<error.h>
#include<sys/mman.h>
#include<../include/DatosMemCompartida.h>
#include "Socket.h"

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000


//#include "Esfera.h"
//#include "Raqueta.h"
#include "DatosMemCompartida.h"
class CMundo  
{
public:
	void Init();
	CMundo();
	virtual ~CMundo();	
	
	void InitGL();	
	void OnKeyboardDown(unsigned char key, int x, int y);
	void OnTimer(int value);
	void OnDraw();	

	
	std::vector<Plano> paredes;

	std::vector<Esfera*> ListaEsferas;

	Plano fondo_izq;
	Plano fondo_dcho;
	Raqueta jugador1;
	Raqueta jugador2;

	int puntos1;
	int puntos2;

	int fd;
	
	char buffer[100];
	char sprintf_buffer[10];

	char buffer_CS[200];
	char sprintf_buffer_CS[10];

	DatosMemCompartida* SharedMem_pointer;
	DatosMemCompartida SharedMem_attribute;

	int fd_SM;
	void* org;

	
	Socket socket_cliente;
	
	char nom_cliente[100];


	 
};

#endif // !defined(AFX_MUNDO_H__9510340A_3D75_485F_93DC_302A43B8039A__INCLUDED_)
