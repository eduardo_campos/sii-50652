//Logger.cpp: implementation of the Logger program.
//
//////////////////////////////////////////////////////////////////////

#include<sys/types.h>
#include<fcntl.h>
#include<sys/stat.h>
#include<stdio.h>
#include<unistd.h>
#include<stdlib.h>
#include<error.h>


int main(){

int reg=0;

reg=mkfifo("/tmp/mififo1",0777);


if(reg<0){
	perror("ERROR AL CREAR LA TUBERIA FIFO");
	exit(-1);
}

printf("Logger Inicializado, Tuberia creada con exito\n");

int fd = open("/tmp/mififo1",O_RDONLY);

if(fd<0){
	perror("ERROR AL ABRIR LA TUBERIA PARA LECTURA (PROGRAMA LOGGER)");
	exit(-1);
}

char buffer[100];

int read_reg=0;


/*
for(;;){

read_reg = read(fd,buffer,sizeof(buffer));  

if(read_reg==-1){
	perror("ERROR AL LEER LA INFORMACION DE LA TUBERIA (PROGRAMA LOGGER)");
	
	break;
}

	printf("%s\n",buffer);


}
*/

while(read(fd,buffer,sizeof(buffer))>0)
	printf("%s\n",buffer);

close(fd);
unlink ("/tmp/mififo1");


}
