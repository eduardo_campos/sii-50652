// Mundo.cpp: implementation of the CMundo class.
//
//////////////////////////////////////////////////////////////////////
#include <fstream>
#include "MundoCliente.h"

#include "glut.h"

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <math.h>

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CMundo::CMundo()
{
	Init();
}

CMundo::~CMundo()
{

	munmap(SharedMem_pointer,sizeof(SharedMem_attribute));

}

void CMundo::InitGL()
{
	//Habilitamos las luces, la renderizacion y el color de los materiales
	glEnable(GL_LIGHT0);
	glEnable(GL_LIGHTING);
	glEnable(GL_DEPTH_TEST);
	glEnable(GL_COLOR_MATERIAL);
	
	glMatrixMode(GL_PROJECTION);
	gluPerspective( 40.0, 800/600.0f, 0.1, 150);
}

void print(char *mensaje, int x, int y, float r, float g, float b)
{
	glDisable (GL_LIGHTING);

	glMatrixMode(GL_TEXTURE);
	glPushMatrix();
	glLoadIdentity();

	glMatrixMode(GL_PROJECTION);
	glPushMatrix();
	glLoadIdentity();
	gluOrtho2D(0, glutGet(GLUT_WINDOW_WIDTH), 0, glutGet(GLUT_WINDOW_HEIGHT) );

	glMatrixMode(GL_MODELVIEW);
	glPushMatrix();
	glLoadIdentity();

	glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
	glDisable(GL_DEPTH_TEST);
	glDisable(GL_BLEND);
	glColor3f(r,g,b);
	glRasterPos3f(x, glutGet(GLUT_WINDOW_HEIGHT)-18-y, 0);
	int len = strlen (mensaje );
	for (int i = 0; i < len; i++) 
		glutBitmapCharacter (GLUT_BITMAP_HELVETICA_18, mensaje[i] );
		
	glMatrixMode(GL_TEXTURE);
	glPopMatrix();

	glMatrixMode(GL_PROJECTION);
	glPopMatrix();

	glMatrixMode(GL_MODELVIEW);
	glPopMatrix();

	glEnable( GL_DEPTH_TEST );
}
void CMundo::OnDraw()
{

//Borrado de la pantalla	
   	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	//Para definir el punto de vista
	glMatrixMode(GL_MODELVIEW);	
	glLoadIdentity();
	
	gluLookAt(0.0, 0, 17,  // posicion del ojo
		0.0, 0.0, 0.0,      // hacia que punto mira  (0,0,0) 
		0.0, 1.0, 0.0);      // definimos hacia arriba (eje Y)    

	/////////////////
	///////////
	//		AQUI EMPIEZA MI DIBUJO
	char cad[100];
	sprintf(cad,"Jugador1: %d",puntos1);
	print(cad,10,0,1,1,1);
	sprintf(cad,"Jugador2: %d",puntos2);
	print(cad,650,0,1,1,1);
	int i;
	for(i=0;i<paredes.size();i++)
		paredes[i].Dibuja();

	fondo_izq.Dibuja();
	fondo_dcho.Dibuja();
	jugador1.Dibuja();
	jugador2.Dibuja();

	for(int m=0;m<ListaEsferas.size();m++)
		ListaEsferas[m]->Dibuja();

	/////////////////
	///////////
	//		AQUI TERMINA MI DIBUJO
	////////////////////////////

	//Al final, cambiar el buffer
	glutSwapBuffers();
}

void CMundo::OnTimer(int value)
{	
	

	/// Actualizacion de datos desde el servidor por el Socket

	socket_cliente.Receive(buffer_CS,sizeof(buffer_CS));

	sscanf(buffer_CS,"%f %f %f %f %f %f %f %f %f %f %f %d %d",&ListaEsferas[0]->centro.x,&ListaEsferas[0]->centro.y,&ListaEsferas[0]->radio,&jugador1.x1,&jugador1.y1,&jugador1.x2,&jugador1.y2,&jugador2.x1,&jugador2.y1,&jugador2.x2,&jugador2.y2,&puntos1,&puntos2);


	///// Atualizacion de datos de memoria compartida
/* DESACTIVACION DEL BOT
	
	SharedMem_pointer->esfera=*ListaEsferas[0];
	SharedMem_pointer->raqueta1=jugador2;

*/
	// Finalización del programa

	if((puntos1==20)||(puntos2==20)){
		if(puntos1>puntos2)
			printf("El jugador 1 ha ganado\n");
		else
			printf("El jugador 2 ha ganado\n");

		
		system("PAUSE");
		munmap(SharedMem_pointer,sizeof(SharedMem_attribute));
		exit(0);
	
	//Envio de la informacion del bot:
	



} 
/*   // DESACTIVACION DEL BOT

	if(SharedMem_pointer->accion==1){
		//write(fd_tecla,"o",sizeof("o"));
		socket_cliente.Send("o",sizeof("o"));
	}
	
	if(SharedMem_pointer->accion==-1){
		//write(fd_tecla,"l",sizeof("l"));
		socket_cliente.Send("l",sizeof("l"));
	}
		
	*/
	
}

void CMundo::OnKeyboardDown(unsigned char key, int x, int y)
{


	switch(key)
	{

	case 's':socket_cliente.Send("s",sizeof("s")); 
	case 'w':socket_cliente.Send("w",sizeof("w"));
	case 'l':socket_cliente.Send("l",sizeof("l")); 
	case 'o':socket_cliente.Send("o",sizeof("o")); 


	}

}

void CMundo::Init()
{

/// Identificacion del cliente

printf("Introduzaca el nombre de su jugador:\n");
gets(nom_cliente);
printf("\n");



//------------------------------

	Plano p;
//pared inferior
	p.x1=-7;p.y1=-5;
	p.x2=7;p.y2=-5;
	paredes.push_back(p);

//superior
	p.x1=-7;p.y1=5;
	p.x2=7;p.y2=5;
	paredes.push_back(p);

	fondo_izq.r=0;
	fondo_izq.x1=-7;fondo_izq.y1=-5;
	fondo_izq.x2=-7;fondo_izq.y2=5;

	fondo_dcho.r=0;
	fondo_dcho.x1=7;fondo_dcho.y1=-5;
	fondo_dcho.x2=7;fondo_dcho.y2=5;

	//a la izq
	jugador1.g=0;
	jugador1.x1=-6;jugador1.y1=-1;
	jugador1.x2=-6;jugador1.y2=1;

	//a la dcha
	jugador2.g=0;
	jugador2.x1=6;jugador2.y1=-1;
	jugador2.x2=6;jugador2.y2=1;
//Esfera

	ListaEsferas.push_back(new Esfera(0.5f,0.0f,0.0f,4.0f,4.0f));

//Creación de un fichero compartido

/* DESACTIVACION DEL BOT 

if((fd_SM = open("/tmp/DatosTenis.txt",O_CREAT|O_RDWR|O_TRUNC,0777))<0){  // CUIDADO CON LOS PARENTESIS
perror("NO PUEDE CREARSE EL FICHERO (MUNDO)");
exit(-1);
}


write(fd_SM,&(SharedMem_attribute),sizeof(DatosMemCompartida));  // Hace falta que haya algo escrito!!

printf("Descriptor de fichero fd_SM: %d\n",fd_SM);


if((org = mmap(NULL,sizeof(DatosMemCompartida),PROT_READ|PROT_WRITE,MAP_SHARED,fd_SM,0))==MAP_FAILED){
	perror("Error en la proyeccion del fichero (MUNDO)");
	close(fd_SM);
	exit(-1);
}
SharedMem_pointer=static_cast<DatosMemCompartida*>(org);

printf("Cliente ha abierto la zona de memoria compartida exitosamente:\n");

close(fd_SM);
*/



/////////// Conexion a socket

socket_cliente.Connect("127.0.0.1",4000);

printf("Cliente conectado a servidor:\n");

socket_cliente.Send(nom_cliente,sizeof(nom_cliente));

}
