// Mundo.cpp: implementation of the CMundo class.
//
//////////////////////////////////////////////////////////////////////
#include <fstream>
#include "MundoServidor.h"

#include "glut.h"

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <math.h>

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////
CMundo::CMundo()
{
	num_clientes=0;
	acabar=false;
	pause=true;
	Init();
}

CMundo::~CMundo()
{
	close(fd);
	

}

////////////////////////////////////// FUNCIONES DE LOS TRHEADS //////////////////////////////

void* hilo_comandos1(void* d)
{
      CMundo* p=(CMundo*) d;
      p->RecibeComandosJugador1();
}

void* hilo_comandos2(void* d)
{
      CMundo* p=(CMundo*) d;
      p->RecibeComandosJugador2();
}


void* hilo_conexiones(void* d)
{

      CMundo* p=(CMundo*) d;
      p->GestionaConexiones();
}

//////////////////////////////////////////////////////////////////////////////////////////////

void CMundo::InitGL()
{
	//Habilitamos las luces, la renderizacion y el color de los materiales
	glEnable(GL_LIGHT0);
	glEnable(GL_LIGHTING);
	glEnable(GL_DEPTH_TEST);
	glEnable(GL_COLOR_MATERIAL);
	
	glMatrixMode(GL_PROJECTION);
	gluPerspective( 40.0, 800/600.0f, 0.1, 150);
}

void print(char *mensaje, int x, int y, float r, float g, float b)
{
	glDisable (GL_LIGHTING);

	glMatrixMode(GL_TEXTURE);
	glPushMatrix();
	glLoadIdentity();

	glMatrixMode(GL_PROJECTION);
	glPushMatrix();
	glLoadIdentity();
	gluOrtho2D(0, glutGet(GLUT_WINDOW_WIDTH), 0, glutGet(GLUT_WINDOW_HEIGHT) );

	glMatrixMode(GL_MODELVIEW);
	glPushMatrix();
	glLoadIdentity();

	glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
	glDisable(GL_DEPTH_TEST);
	glDisable(GL_BLEND);
	glColor3f(r,g,b);
	glRasterPos3f(x, glutGet(GLUT_WINDOW_HEIGHT)-18-y, 0);
	int len = strlen (mensaje );
	for (int i = 0; i < len; i++) 
		glutBitmapCharacter (GLUT_BITMAP_HELVETICA_18, mensaje[i] );
		
	glMatrixMode(GL_TEXTURE);
	glPopMatrix();

	glMatrixMode(GL_PROJECTION);
	glPopMatrix();

	glMatrixMode(GL_MODELVIEW);
	glPopMatrix();

	glEnable( GL_DEPTH_TEST );
}
void CMundo::OnDraw()
{
	//Borrado de la pantalla	
   	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	//Para definir el punto de vista
	glMatrixMode(GL_MODELVIEW);	
	glLoadIdentity();
	
	gluLookAt(0.0, 0, 17,  // posicion del ojo
		0.0, 0.0, 0.0,      // hacia que punto mira  (0,0,0) 
		0.0, 1.0, 0.0);      // definimos hacia arriba (eje Y)    

	/////////////////
	///////////
	//		AQUI EMPIEZA MI DIBUJO
	char cad[100];
	sprintf(cad,"Jugador1: %d",puntos1);
	print(cad,10,0,1,1,1);
	sprintf(cad,"Jugador2: %d",puntos2);
	print(cad,650,0,1,1,1);
	int i;
	for(i=0;i<paredes.size();i++)
		paredes[i].Dibuja();

	fondo_izq.Dibuja();
	fondo_dcho.Dibuja();
	jugador1.Dibuja();
	jugador2.Dibuja();

	for(int m=0;m<ListaEsferas.size();m++)
		ListaEsferas[m]->Dibuja();

	/////////////////
	///////////
	//		AQUI TERMINA MI DIBUJO
	////////////////////////////

	//Al final, cambiar el buffer
	glutSwapBuffers();
}

void CMundo::OnTimer(int value)
{	

if(num_clientes>=2)
	pause=false;

if(pause!=true){


	jugador1.Mueve(0.025f);
	jugador2.Mueve(0.025f);

	for(int m=0;m<ListaEsferas.size();m++){
		ListaEsferas[m]->Mueve(0.025f);
		jugador1.Rebota(*ListaEsferas[m]);
		jugador2.Rebota(*ListaEsferas[m]);
	}
	
	for(int i=0;i<paredes.size();i++){
		for(int m=0;m<ListaEsferas.size();m++)
		{
			paredes[i].Rebota(*ListaEsferas[m]);

	
		}
	}




	for(int i=0;i<paredes.size();i++)
	{
		paredes[i].Rebota(jugador1);
		paredes[i].Rebota(jugador2);
	}

	for(int m=0;m<ListaEsferas.size();m++){
		if(fondo_izq.Rebota(*ListaEsferas[m]))
		{
			ListaEsferas[m]->centro.x=0;
			ListaEsferas[m]->centro.y=rand()/(float)RAND_MAX;
			ListaEsferas[m]->velocidad.x=4+2*rand()/(float)RAND_MAX;
			ListaEsferas[m]->velocidad.y=4+2*rand()/(float)RAND_MAX;
			puntos2++;

			ListaEsferas[m]->radio=0.5f;

		/*	if(ListaEsferas.size()<6)
				ListaEsferas.push_back(new Esfera(0.5f,0.0f,0.0f,float(rand()%9-4),float(rand()%9-4))); */

		// ENVIO A Logger
			
			char jug[]="El Jugador 2 marca 1 punto, lleva un total de ";
			strcpy(buffer,jug); //Borrado de datos anteriores
			sprintf(sprintf_buffer,"%d",puntos2);
			strcat(buffer,sprintf_buffer);
			strcat(buffer," puntos.");

			write(fd,buffer,sizeof(buffer));

		}
	}

	for(int m=0;m<ListaEsferas.size();m++){
		if(fondo_dcho.Rebota(*ListaEsferas[m]))
		{
			ListaEsferas[m]->centro.x=0;
			ListaEsferas[m]->centro.y=rand()/(float)RAND_MAX;
			ListaEsferas[m]->velocidad.x=-4-2*rand()/(float)RAND_MAX;
			ListaEsferas[m]->velocidad.y=-4-2*rand()/(float)RAND_MAX;
			puntos1++;

			ListaEsferas[m]->radio=0.5f;

		/*	if(ListaEsferas.size()<6)
				ListaEsferas.push_back(new Esfera(0.5f,0.0f,0.0f,float(rand()%9-4),float(rand()%9-4))); */
		// ENVIO A Logger

			char jug[]="El Jugador 1 marca 1 punto, lleva un total de ";
			strcpy(buffer,jug); // Borrado de datos anteriores
			sprintf(sprintf_buffer,"%d",puntos1);
			strcat(buffer,sprintf_buffer);
			strcat(buffer," puntos.");

			write(fd,buffer,sizeof(buffer));

		}
	}




	// Finalización del programa

	if((puntos1==100)||(puntos2==100)){
		if(puntos1>puntos2)
			printf("El jugador 1 ha ganado\n");
		else
			printf("El jugador 2 ha ganado\n");

		
		exit(0);


} 


	// Envio de datos al cliente por el socket

	sprintf(buffer_CS,"%f %f %f %f %f %f %f %f %f %f %f %d %d",ListaEsferas[0]->centro.x,ListaEsferas[0]->centro.y,ListaEsferas[0]->radio, jugador1.x1,jugador1.y1,jugador1.x2,jugador1.y2,jugador2.x1,jugador2.y1,jugador2.x2,jugador2.y2,puntos1,puntos2);

	for (int i=socket_comunicacion.size()-1; i>=0; i--) {
      	   if (socket_comunicacion[i].Send(buffer_CS,sizeof(buffer_CS)) <= 0) {
       	     socket_comunicacion.erase(socket_comunicacion.begin()+i);
		num_clientes--;
       	     if (i < 2){ // Hay menos de dos clientes conectados
	
              // Se resetean los puntos a cero, la posicion de la esfera:
		puntos1=0;
		puntos2=0;
			ListaEsferas[0]->centro.x=0;
			ListaEsferas[0]->centro.y=rand()/(float)RAND_MAX;
			ListaEsferas[0]->velocidad.x=-4-2*rand()/(float)RAND_MAX;
			ListaEsferas[0]->velocidad.y=-4-2*rand()/(float)RAND_MAX;
		pause=true;
		

		}
       		  }
   	  }
  	



}/// pause





	
}

void CMundo::OnKeyboardDown(unsigned char key, int x, int y)
{ // El servidor no es un jugador

}

void CMundo::Init()
{

	Plano p;
//pared inferior
	p.x1=-7;p.y1=-5;
	p.x2=7;p.y2=-5;
	paredes.push_back(p);

//superior
	p.x1=-7;p.y1=5;
	p.x2=7;p.y2=5;
	paredes.push_back(p);

	fondo_izq.r=0;
	fondo_izq.x1=-7;fondo_izq.y1=-5;
	fondo_izq.x2=-7;fondo_izq.y2=5;

	fondo_dcho.r=0;
	fondo_dcho.x1=7;fondo_dcho.y1=-5;
	fondo_dcho.x2=7;fondo_dcho.y2=5;

	//a la izq
	jugador1.g=0;
	jugador1.x1=-6;jugador1.y1=-1;
	jugador1.x2=-6;jugador1.y2=1;

	//a la dcha
	jugador2.g=0;
	jugador2.x1=6;jugador2.y1=-1;
	jugador2.x2=6;jugador2.y2=1;
//Esfera

	ListaEsferas.push_back(new Esfera(0.5f,0.0f,0.0f,4.0f,4.0f));


 //logger

if((fd = open("/tmp/mififo1",O_WRONLY))<0){
	perror("ERROR AL ABRIR LA TUBERIA PARA ESCRITURA Logger (PROGRAMA SERVIDOR)");
	exit(-1);
}

printf("Tuberia logger abierta:\n");

//Creación de un fichero compartido


// De esto se encarga el cliente




////// CONFIGURACION DEL SOCKET DE CONEXION

printf("Inicializacion de sockets:\n");

if(socket_servidor_conexion.InitServer("127.0.0.1",4000)<0){
	perror("Error al inicializar Socket Servidor:\n");
	exit(-1);
}



pthread_create(&p_CS1, NULL,hilo_comandos1,this);
pthread_create(&p_CS2, NULL,hilo_comandos2,this);


pthread_create(&p_Conexiones, NULL,hilo_conexiones,this);

	

}



void CMundo::RecibeComandosJugador1()
{


printf("El thread de las teclas está funcionando\n");
     while (1) {
	char cad[100];
     
	usleep(25000);

if(num_clientes>=2){
     /*for (int i=socket_comunicacion.size()-1; i>=0; i--) {
         if(i==0){*/

		socket_comunicacion[0].Receive(cad,sizeof(cad));

	    unsigned char key;
            sscanf(cad,"%c",&key);

		
           		if(key=='s')jugador1.velocidad.y=-4;
  			if(key=='w')jugador1.velocidad.y=4;
            		if(key=='l')jugador1.velocidad.y=-4;
            		if(key=='o')jugador1.velocidad.y=4;
			

           
          

	//}
        	
     	
  	//}
}

 	}
}


void CMundo::RecibeComandosJugador2()
{


printf("El thread de las teclas está funcionando\n");
     while (1) {
	char cad[100];

     usleep(25000);

if(num_clientes>=2){

     /*for (int i=socket_comunicacion.size()-1; i>=0; i--) {
         if(i==1){ */

		socket_comunicacion[1].Receive(cad,sizeof(cad));

	    unsigned char key;
            sscanf(cad,"%c",&key);

		
		
            		if(key=='l')jugador2.velocidad.y=-4;
            		if(key=='o')jugador2.velocidad.y=4;
            		if(key=='s')jugador2.velocidad.y=-4;
            		if(key=='w')jugador2.velocidad.y=4;
			
           
          

	//}
        	
     	
  	//}
}

 	}
}





void CMundo::GestionaConexiones()
{

printf("El thread de gestion de conexiones esta funcionando:\n");
	
	int i=0;

	while(1){

	if(num_clientes>=2)
		pause=false;
	
	if(acabar==false){
		socket_comunicacion.push_back(socket_servidor_conexion.Accept());
		socket_comunicacion[i].Receive(nom_cliente,sizeof(nom_cliente));
		printf("El usuario %s, se ha conectado:\n",nom_cliente);
		i++;
		num_clientes++;
	}
	else break;
	}

}
