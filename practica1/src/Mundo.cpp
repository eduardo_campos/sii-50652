// Mundo.cpp: implementation of the CMundo class.
//
//////////////////////////////////////////////////////////////////////
#include <fstream>
#include "Mundo.h"
#include "glut.h"

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <math.h>

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CMundo::CMundo()
{
	Init();
}

CMundo::~CMundo()
{
	close(fd);
	//unlink ("/tmp/mififo1");
	munmap(SharedMem_pointer,sizeof(SharedMem_attribute));

	

}

void CMundo::InitGL()
{
	//Habilitamos las luces, la renderizacion y el color de los materiales
	glEnable(GL_LIGHT0);
	glEnable(GL_LIGHTING);
	glEnable(GL_DEPTH_TEST);
	glEnable(GL_COLOR_MATERIAL);
	
	glMatrixMode(GL_PROJECTION);
	gluPerspective( 40.0, 800/600.0f, 0.1, 150);
}

void print(char *mensaje, int x, int y, float r, float g, float b)
{
	glDisable (GL_LIGHTING);

	glMatrixMode(GL_TEXTURE);
	glPushMatrix();
	glLoadIdentity();

	glMatrixMode(GL_PROJECTION);
	glPushMatrix();
	glLoadIdentity();
	gluOrtho2D(0, glutGet(GLUT_WINDOW_WIDTH), 0, glutGet(GLUT_WINDOW_HEIGHT) );

	glMatrixMode(GL_MODELVIEW);
	glPushMatrix();
	glLoadIdentity();

	glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
	glDisable(GL_DEPTH_TEST);
	glDisable(GL_BLEND);
	glColor3f(r,g,b);
	glRasterPos3f(x, glutGet(GLUT_WINDOW_HEIGHT)-18-y, 0);
	int len = strlen (mensaje );
	for (int i = 0; i < len; i++) 
		glutBitmapCharacter (GLUT_BITMAP_HELVETICA_18, mensaje[i] );
		
	glMatrixMode(GL_TEXTURE);
	glPopMatrix();

	glMatrixMode(GL_PROJECTION);
	glPopMatrix();

	glMatrixMode(GL_MODELVIEW);
	glPopMatrix();

	glEnable( GL_DEPTH_TEST );
}
void CMundo::OnDraw()
{
	//Borrado de la pantalla	
   	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	//Para definir el punto de vista
	glMatrixMode(GL_MODELVIEW);	
	glLoadIdentity();
	
	gluLookAt(0.0, 0, 17,  // posicion del ojo
		0.0, 0.0, 0.0,      // hacia que punto mira  (0,0,0) 
		0.0, 1.0, 0.0);      // definimos hacia arriba (eje Y)    

	/////////////////
	///////////
	//		AQUI EMPIEZA MI DIBUJO
	char cad[100];
	sprintf(cad,"Jugador1: %d",puntos1);
	print(cad,10,0,1,1,1);
	sprintf(cad,"Jugador2: %d",puntos2);
	print(cad,650,0,1,1,1);
	int i;
	for(i=0;i<paredes.size();i++)
		paredes[i].Dibuja();

	fondo_izq.Dibuja();
	fondo_dcho.Dibuja();
	jugador1.Dibuja();
	jugador2.Dibuja();

	for(int m=0;m<ListaEsferas.size();m++)
		ListaEsferas[m]->Dibuja();

	/////////////////
	///////////
	//		AQUI TERMINA MI DIBUJO
	////////////////////////////

	//Al final, cambiar el buffer
	glutSwapBuffers();
}

void CMundo::OnTimer(int value)
{	
	jugador1.Mueve(0.025f);
	jugador2.Mueve(0.025f);

	for(int m=0;m<ListaEsferas.size();m++){
		ListaEsferas[m]->Mueve(0.025f);
		jugador1.Rebota(*ListaEsferas[m]);
		jugador2.Rebota(*ListaEsferas[m]);
	}
	
	for(int i=0;i<paredes.size();i++){
		for(int m=0;m<ListaEsferas.size();m++)
		{
			paredes[i].Rebota(*ListaEsferas[m]);

	
		}
	}




	for(int i=0;i<paredes.size();i++)
	{
		paredes[i].Rebota(jugador1);
		paredes[i].Rebota(jugador2);
	}

	for(int m=0;m<ListaEsferas.size();m++){
		if(fondo_izq.Rebota(*ListaEsferas[m]))
		{
			ListaEsferas[m]->centro.x=0;
			ListaEsferas[m]->centro.y=rand()/(float)RAND_MAX;
			ListaEsferas[m]->velocidad.x=4+2*rand()/(float)RAND_MAX;
			ListaEsferas[m]->velocidad.y=4+2*rand()/(float)RAND_MAX;
			puntos2++;

			ListaEsferas[m]->radio=0.5f;

		/*	if(ListaEsferas.size()<6)
				ListaEsferas.push_back(new Esfera(0.5f,0.0f,0.0f,float(rand()%9-4),float(rand()%9-4))); */

		// ENVIO A TUBERIAS
			
			char jug[]="El Jugador 2 marca 1 punto, lleva un total de ";
			strcpy(buffer,jug); //Borrado de datos anteriores
			sprintf(sprintf_buffer,"%d",puntos2);
			strcat(buffer,sprintf_buffer);
			strcat(buffer," puntos.");

			write(fd,buffer,sizeof(buffer));

		}
	}

	for(int m=0;m<ListaEsferas.size();m++){
		if(fondo_dcho.Rebota(*ListaEsferas[m]))
		{
			ListaEsferas[m]->centro.x=0;
			ListaEsferas[m]->centro.y=rand()/(float)RAND_MAX;
			ListaEsferas[m]->velocidad.x=-4-2*rand()/(float)RAND_MAX;
			ListaEsferas[m]->velocidad.y=-4-2*rand()/(float)RAND_MAX;
			puntos1++;

			ListaEsferas[m]->radio=0.5f;

		/*	if(ListaEsferas.size()<6)
				ListaEsferas.push_back(new Esfera(0.5f,0.0f,0.0f,float(rand()%9-4),float(rand()%9-4))); */
		// ENVIO A TUBERIAS

			char jug[]="El Jugador 1 marca 1 punto, lleva un total de ";
			strcpy(buffer,jug); // Borrado de datos anteriores
			sprintf(sprintf_buffer,"%d",puntos1);
			strcat(buffer,sprintf_buffer);
			strcat(buffer," puntos.");

			write(fd,buffer,sizeof(buffer));

		}
	}



	///// Atualizacion de datos de memoria compartida

	
	SharedMem_pointer->esfera=*ListaEsferas[0];
	SharedMem_pointer->raqueta1=jugador2;

	///// Ejecución de movimiento

	if(SharedMem_pointer->accion==1)
	CMundo::OnKeyboardDown('o',0,0);
	if(SharedMem_pointer->accion==-1)
	CMundo::OnKeyboardDown('l',0,0);

	// Finalización del programa

	if((puntos1==3)||(puntos2==3)){
		if(puntos1>puntos2)
			printf("El jugador 1 ha ganado\n");
		else
			printf("El jugador 2 ha ganado\n");

		
		system("PAUSE");
		munmap(SharedMem_pointer,sizeof(SharedMem_attribute));
		exit(0);


} 
	
}

void CMundo::OnKeyboardDown(unsigned char key, int x, int y)
{
	switch(key)
	{
//	case 'a':jugador1.velocidad.x=-1;break;
//	case 'd':jugador1.velocidad.x=1;break;
	case 's':jugador1.velocidad.y=-4;break;
	case 'w':jugador1.velocidad.y=4;break;
	case 'l':jugador2.velocidad.y=-4;break;
	case 'o':jugador2.velocidad.y=4;break;

	}
}

void CMundo::Init()
{

	Plano p;
//pared inferior
	p.x1=-7;p.y1=-5;
	p.x2=7;p.y2=-5;
	paredes.push_back(p);

//superior
	p.x1=-7;p.y1=5;
	p.x2=7;p.y2=5;
	paredes.push_back(p);

	fondo_izq.r=0;
	fondo_izq.x1=-7;fondo_izq.y1=-5;
	fondo_izq.x2=-7;fondo_izq.y2=5;

	fondo_dcho.r=0;
	fondo_dcho.x1=7;fondo_dcho.y1=-5;
	fondo_dcho.x2=7;fondo_dcho.y2=5;

	//a la izq
	jugador1.g=0;
	jugador1.x1=-6;jugador1.y1=-1;
	jugador1.x2=-6;jugador1.y2=1;

	//a la dcha
	jugador2.g=0;
	jugador2.x1=6;jugador2.y1=-1;
	jugador2.x2=6;jugador2.y2=1;
//Esfera

	ListaEsferas.push_back(new Esfera(0.5f,0.0f,0.0f,4.0f,4.0f));

/*
			ListaEsferas[m]->centro.x=0;
			ListaEsferas[m]->centro.y=rand()/(float)RAND_MAX;
			ListaEsferas[m]->velocidad.x=4+2*rand()/(float)RAND_MAX;
			ListaEsferas[m]->velocidad.y=4+2*rand()/(float)RAND_MAX
*/

//Apertura de tuberia

fd = open("/tmp/mififo1",O_WRONLY);

if(fd<0){
	perror("ERROR AL ABRIR LA TUBERIA PARA ESCRITURA (PROGRAMA TENIS)");
	exit(-1);
}


//Creación de un fichero compartido


if((fd_SM = open("/tmp/DatosTenis.txt",O_CREAT|O_RDWR|O_TRUNC,0777))<0){  // CUIDADO CON LOS PARENTESIS
perror("NO PUEDE CREARSE EL FICHERO (MUNDO)");
exit(-1);
}


write(fd_SM,&(SharedMem_attribute),sizeof(DatosMemCompartida));  // Hace falta que haya algo escrito!!

printf("Descriptor de fichero fd_SM: %d\n",fd_SM);


if((org = mmap(NULL,sizeof(DatosMemCompartida),PROT_READ|PROT_WRITE,MAP_SHARED,fd_SM,0))==MAP_FAILED){
	perror("Error en la proyeccion del fichero (MUNDO)");
	close(fd_SM);
	exit(-1);
}
SharedMem_pointer=static_cast<DatosMemCompartida*>(org);

printf("Tenis ha abierto la zona de memoria compartida exitosamente:\n");

close(fd_SM);




}
